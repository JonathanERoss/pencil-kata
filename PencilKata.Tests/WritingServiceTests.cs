using System;
using NUnit.Framework;

namespace PencilKata.Tests
{
    [TestFixture]
    public class WritingServiceTests
    {
        private IWritingService _writingService;

        [SetUp]
        public void SetUp()
        {
            _writingService = new WritingService();
            _writingService.AssignPencil(PencilsForTesting.BasicTestPencil);
        }

        [Test]
        public void AssignedPencilCanBeRetreived()
        {
            Pencil expectedPencil = PencilsForTesting.BasicTestPencil;
            _writingService.AssignPencil(expectedPencil);

            Pencil usedPencil = _writingService.GetPencil();

            Assert.AreEqual(expectedPencil, usedPencil);
        }

        [Test]
        public void AssignedPaperCanBeRetreived()
        {
            string expectedPaper = "Something interesting";
            _writingService.AssignPaper(expectedPaper);

            string usedPaper = _writingService.GetPaper();

            Assert.AreEqual(expectedPaper, usedPaper);
        }

        [Test]
        public void PencilsWhichCannotEraseCannotBeAssigned()
        {
            Assert.Throws(typeof(InvalidOperationException),
                delegate ()
                {
                    _writingService.AssignPencil(PencilsForTesting.PencilThatCannotErase);
                }, "The Writing Service cannot be assigned a Pencil which cannot erase");
        }

        [Test]
        public void PencilsWhichCannotBeSharpenedCannotBeAssigned()
        {
            Assert.Throws(typeof(InvalidOperationException),
                delegate ()
                {
                    _writingService.AssignPencil(PencilsForTesting.PencilThatCannotBeSharpened);
                }, "The Writing Service cannot be assigned a Pencil which cannot be sharpened");
        }

        [Test]
        public void PencilsWhichCannotWriteCannotBeAssigned()
        {
            Assert.Throws(typeof(InvalidOperationException),
                delegate ()
                {
                    _writingService.AssignPencil(PencilsForTesting.PencilThatCannotWrite);
                }, "The Writing Service cannot be assigned a Pencil which cannot write");
        }

        [Test]
        public void PencilsWhichAreDullCanBeAssigned()
        {
            Pencil testPencil = PencilsForTesting.PencilThatIsDull;
            _writingService.AssignPencil(testPencil);

            Pencil expectedPencil = _writingService.GetPencil();

            Assert.AreEqual(testPencil, expectedPencil);
        }

        [Test]
        public void AssignedPencilPointHasDegradedAfterWriting()
        {
            _writingService.AssignPaper();
            string firstText = "She sells sea shells";
            string lastText = " down by the sea shore";
            int originalPointDurability = _writingService.GetPencil().Point.CurrentDurability;

            _writingService.WriteTextOnPaper(firstText);
            _writingService.WriteTextOnPaper(lastText);

            int finalPointDurability = _writingService.GetPencil().Point.CurrentDurability;
            Assert.IsTrue(finalPointDurability < originalPointDurability);
        }

        [Test]
        public void AssignedPencilEraserHasDegradedAfterErasing()
        {
            Pencil testPencil = new Pencil(10, 20, 50);
            _writingService.AssignPencil(testPencil);
            string testPaper = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?";
            _writingService.AssignPaper(testPaper);
            int originalEraserDurability = _writingService.GetPencil().Eraser.CurrentDurability;
            string textToErase = "woodchuck";

            _writingService.EraseText(textToErase);
            _writingService.EraseText(textToErase);

            int finalEraserDurability = _writingService.GetPencil().Eraser.CurrentDurability;
            Assert.IsTrue(finalEraserDurability < originalEraserDurability);
        }

        [Test]
        public void AssignedPencilCanWriteThenWillDegradeAndCanBeSharpenedThenCanWriteAgain()
        {
            Pencil testPencil = new Pencil(30, 20, 50);
            _writingService.AssignPencil(testPencil);
            _writingService.AssignPaper();
            string textToWrite = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?";

            // Write some text on the paper
            _writingService.WriteTextOnPaper(textToWrite);
            string expectedText = "How much wood would a woodchuck chu                                   ";
            Assert.AreEqual(expectedText, _writingService.GetPaper(), "The expected portion of the text was written");

            // Sharpen the pencil so we can write again
            testPencil = _writingService.GetPencil();
            Assert.IsTrue(testPencil.Point.IsDegraded(), "Point is degraded after writing");
            Assert.IsTrue(testPencil.CanBeSharpened(), "Pencil can be sharpened");
            testPencil.Sharpen();
            Assert.IsFalse(testPencil.Point.IsDegraded(), "Point is no longer degraded after writing");
            _writingService.AssignPencil(testPencil);

            // Write some more text onto the paper, resuming where we left off
            textToWrite = "ck if a woodchuck could chuck wood?";
            _writingService.EditText("    ", textToWrite);
            expectedText = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?";
            Assert.AreEqual(expectedText, _writingService.GetPaper(), "The expected portion of the text was written");
        }

        [Test]
        public void AssignedPencilPointHasDegradedAfterEditing()
        {
            _writingService.AssignPaper();
            _writingService.WriteTextOnPaper("I have a red shirt");
            int pointDurabilityBeforeEdit = _writingService.GetPencil().Point.CurrentDurability;

            _writingService.EditText("red", "yellow");
            int finalPointDurability = _writingService.GetPencil().Point.CurrentDurability;

            Assert.IsTrue(finalPointDurability < pointDurabilityBeforeEdit);
        }

        [Test]
        public void NewTextIsAppendedToExistingText()
        {
            _writingService.AssignPaper();
            string firstText = "She sells sea shells";
            string lastText = " down by the sea shore";

            _writingService.WriteTextOnPaper(firstText);
            _writingService.WriteTextOnPaper(lastText);

            string textOnThePaper = _writingService.GetPaper();
            Assert.AreEqual(textOnThePaper, firstText + lastText);
        }

        [Test]
        public void EraseTextAndInsertNewWordWithoutShiftingAndOverwrittenTextBecomesAtSymbol()
        {
            string testPaper = "An apple a day keeps the doctor away";
            _writingService.AssignPaper(testPaper);

            _writingService.EraseText("apple");
            string expectedTextAfterErase = "An       a day keeps the doctor away";
            Assert.AreEqual(expectedTextAfterErase, _writingService.GetPaper(), "Expected to erase text");

            _writingService.EditText("       ", " artichoke");
            string expectedTextAfterEdit = "An artich@k@ay keeps the doctor away";
            Assert.AreEqual(expectedTextAfterEdit, _writingService.GetPaper(), "Expected character collisions to become @ symbol");
        }

        [Test]
        public void EraseTextFromEndWillReplaceTextWithSpaces()
        {
            Pencil testPencil = new Pencil(10, 20, 50);
            _writingService.AssignPencil(testPencil);
            string testPaper = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?";
            _writingService.AssignPaper(testPaper);

            string textToErase = "wood";
            string expectedTextAfterFirstErase = "How much wood would a woodchuck chuck if a woodchuck could chuck     ?";
            string expectedTextAfterSecondErase = "How much wood would a woodchuck chuck if a     chuck could chuck     ?";
            string expectedTextAfterThirdErase = "How much wood would a     chuck chuck if a     chuck could chuck     ?";

            _writingService.EraseText(textToErase);
            Assert.AreEqual(expectedTextAfterFirstErase, _writingService.GetPaper());
            _writingService.EraseText(textToErase);
            Assert.AreEqual(expectedTextAfterSecondErase, _writingService.GetPaper());
            _writingService.EraseText(textToErase);
            Assert.AreEqual(expectedTextAfterThirdErase, _writingService.GetPaper());
        }
    }
}
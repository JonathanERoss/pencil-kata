using NUnit.Framework;

namespace PencilKata.Tests
{
    [TestFixture]
    public class PencilTests
    {
        [Test]
        public void PencilShouldOnlyHavePointEnabledForRestore()
        {
            Pencil testPencil = PencilsForTesting.BasicTestPencil;

            Assert.IsTrue(testPencil.Point.CanRestoreDurability);
            Assert.IsFalse(testPencil.Length.CanRestoreDurability);
            Assert.IsFalse(testPencil.Eraser.CanRestoreDurability);
        }

        [Test]
        public void PencilCanWriteWhenPointIsNotDegraded()
        {
            Pencil testPencil = PencilsForTesting.BasicTestPencil;

            testPencil.WriteText("A");

            Assert.IsFalse(testPencil.Point.IsDegraded(), "Point is not degraded");
            Assert.IsTrue(testPencil.CanWrite(), "Pencil can write");
        }

        [Test]
        public void PencilCannotWriteWhenPointIsDegraded()
        {
            Pencil testPencil = PencilsForTesting.PencilThatIsDull;

            testPencil.WriteText("A");

            Assert.IsTrue(testPencil.Point.IsDegraded(), "Point is degraded");
            Assert.IsFalse(testPencil.CanWrite(), "Pencil cannot write");
        }

        [Test]
        public void PencilCanBeSharpenedWhenLengthIsNotDegraded()
        {
            Pencil testPencil = PencilsForTesting.BasicTestPencil;

            testPencil.Sharpen();

            Assert.IsFalse(testPencil.Length.IsDegraded(), "Length is not degraded");
            Assert.IsTrue(testPencil.CanBeSharpened(), "Pencil can be sharpened");
        }

        [Test]
        public void PencilCannotBeSharpenedWhenLengthIsDegraded()
        {
            Pencil testPencil = PencilsForTesting.PencilThatCannotBeSharpened;

            testPencil.Sharpen();

            Assert.IsTrue(testPencil.Length.IsDegraded(), "Length is degraded");
            Assert.IsFalse(testPencil.CanBeSharpened(), "Pencil cannot be sharpened");
        }

        [Test]
        public void PencilCanEraseWhenEraserIsNotDegraded()
        {
            Pencil testPencil = new Pencil(10, 20, 6);
            string textToErase = "Buffalo Bill";
            string expectedTextAfterErase = "Buffa       ";

            string actualTextAfterErase = testPencil.EraseText(textToErase);

            Assert.AreEqual(expectedTextAfterErase, actualTextAfterErase);
        }

        [Test]
        public void PencilPointWillDegradeByTwoWhenWritingUppercase()
        {
            Pencil testPencil = PencilsForTesting.BasicTestPencil;
            int currentPointDurability = testPencil.Point.CurrentDurability;

            testPencil.WriteText("ABC");

            int expectedPointDurability = currentPointDurability - 6;
            Assert.IsTrue(testPencil.Point.CurrentDurability == expectedPointDurability);
        }

        [Test]
        public void PencilPointWillDegradeByOneWhenWritingLowercase()
        {
            Pencil testPencil = PencilsForTesting.BasicTestPencil;
            int currentPointDurability = testPencil.Point.CurrentDurability;

            testPencil.WriteText("abc");

            int expectedPointDurability = currentPointDurability - 3;
            Assert.IsTrue(testPencil.Point.CurrentDurability == expectedPointDurability);
        }

        [Test]
        public void PencilPointWillDegradeByZeroWhenWritingSpaces()
        {
            Pencil testPencil = PencilsForTesting.BasicTestPencil;
            int currentPointDurability = testPencil.Point.CurrentDurability;

            testPencil.WriteText("A C");

            int expectedPointDurability = currentPointDurability - 4;
            Assert.IsTrue(testPencil.Point.CurrentDurability == expectedPointDurability);
        }

        [Test]
        public void PencilWillWriteSpacesWhenThePointBecomesDull()
        {
            Pencil testPencil = new Pencil(15, 10, 5);
            int currentPointDurability = testPencil.Point.CurrentDurability;            
            string textToWrite = "Please, sharpen me.  I am a dull pencil.";
            Assert.IsTrue(testPencil.CanWrite());

            string actualTextWritten = testPencil.WriteText(textToWrite);
            string expectedTextWritten = "Please, sharpen                         ";

            Assert.IsFalse(testPencil.CanWrite());
            Assert.AreEqual(expectedTextWritten, actualTextWritten);
        }
    }
}
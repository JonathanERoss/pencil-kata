using System;
using NUnit.Framework;

namespace PencilKata.Tests
{
    [TestFixture]
    public class DurablePropertyTests
    {
        private DurableProperty _testProperty;

        [SetUp]
        public void SetUp()
        {
            _testProperty = new DurableProperty(10);
        }

        [Test]
        public void DurabilityShouldBeReducedByAmountRequested()
        {
            int originalDurability = _testProperty.CurrentDurability;
            int amountToDegrade = 2;
            _testProperty.Degrade(amountToDegrade);

            Assert.IsTrue(_testProperty.CurrentDurability == originalDurability - 2);
        }

        [Test]
        public void DurabilityCannotDegradeBelowZero()
        {
            _testProperty.Degrade(_testProperty.InitialDurability + 2);

            Assert.IsTrue(_testProperty.CurrentDurability == 0);
        }

        [Test]
        public void DurabilityShouldReturnToInitialValueWhenRestoredAndRestoreIsEnabled()
        {
            _testProperty.AllowRestore();
            _testProperty.Degrade();
            _testProperty.Restore();

            Assert.AreEqual(_testProperty.CurrentDurability, _testProperty.InitialDurability);
        }

        [Test]
        public void DurabilityShouldNotReturnToInitialValueWhenRestoredAndRestoreIsNotEnabled()
        {
            _testProperty.DenyRestore();
            _testProperty.Degrade();

            Assert.Throws(typeof(InvalidOperationException),
                delegate ()
                {
                    _testProperty.Restore();
                }, "The Property is not enabled for restore") ;
        }
    }
}
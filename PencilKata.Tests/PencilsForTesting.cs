﻿namespace PencilKata.Tests
{
    /// <summary>
    ///     Static class to help construct pencils for testing
    /// </summary>
    public static class PencilsForTesting
    {
        /// <summary>
        ///     A good pencil for testing
        /// </summary>
        public static Pencil BasicTestPencil => new Pencil(100, 20, 5);

        /// <summary>
        ///     A pencil that cannot erase because it's eraser durability is zero
        /// </summary>
        public static Pencil PencilThatCannotErase => new Pencil(10, 20, 0);

        /// <summary>
        ///     A pencil that cannot be sharpened because it's length durability is zero
        /// </summary>
        public static Pencil PencilThatCannotBeSharpened => new Pencil(10, 0, 5);

        /// <summary>
        ///     A pencil that has a dull point and can be sharpened
        /// </summary>
        public static Pencil PencilThatIsDull => DegradePencil(BasicTestPencil);

        /// <summary>
        ///     A pencil that is dull and cannot be sharpened because both the point and length durability is zero 
        /// </summary>
        public static Pencil PencilThatCannotWrite => new Pencil(0, 0, 5);

        /// <summary>
        ///     Degrade a pencil until it becomes dull
        /// </summary>
        /// <param name="pencil">The pencil to be degraded</param>
        private static Pencil DegradePencil(Pencil pencil)
        {
            while (pencil.CanWrite())
            {
                pencil.Point.Degrade();
            }
            return pencil;
        }
    }
}

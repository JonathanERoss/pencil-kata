﻿using System;
using System.Text;

namespace PencilKata
{
    /// <summary>
    ///     The Writing Service used to work with Pencils.
    /// </summary>
    public class WritingService : IWritingService
    {
        /// <summary>
        ///     The Pencil assigned to the service
        /// </summary>
        private Pencil Pencil { get; set; }

        /// <summary>
        ///     The Paper assigned to the service
        /// </summary>
        private string Paper { get; set; }

        #region Pencil
        /// <summary>
        ///     Assigns a pencil to the Writing Service
        /// </summary>
        /// <param name="pencil">The pencil that will be assigned</param>
        public void AssignPencil(Pencil pencil)
        {
            if (!pencil.CanWrite() && !pencil.CanBeSharpened())
            {
                throw new InvalidOperationException("The Writing Service cannot be assigned a Pencil which cannot write");
            }

            if (!pencil.CanBeSharpened())
            {
                throw new InvalidOperationException("The Writing Service cannot be assigned a Pencil which cannot be sharpened");
            }

            if (!pencil.CanErase())
            {
                throw new InvalidOperationException("The Writing Service cannot be assigned a Pencil which cannot erase");
            }

            Pencil = pencil;
        }

        /// <summary>
        ///     Get a copy of the pencil assigned to the Writing Service
        /// </summary>
        public Pencil GetPencil()
        {
            return Pencil;
        }
        #endregion

        #region Paper
        /// <summary>
        ///     Assigns Paper to the Writing Service
        /// </summary>
        /// <param name="paper">The paper that will be assigned</param>
        public void AssignPaper(string paper = "")
        {
            Paper = paper;
        }

        /// <summary>
        ///     Get a copy of the paper assigned to the Writing Service
        /// </summary>
        public string GetPaper()
        {
            return Paper;
        }
        #endregion

        #region Editing
        /// <summary>
        ///     Write text on the Paper with the assigned Pencil
        /// </summary>
        /// <param name="textToWrite">The text that will be written to the paper with the pencil</param>
        public void WriteTextOnPaper(string textToWrite)
        {
            StringBuilder existingTextOnPaper = new StringBuilder(Paper);
            Paper = existingTextOnPaper.Append(Pencil.WriteText(textToWrite)).ToString();
        }

        /// <summary>
        ///     Erase desired text from end of the paper
        /// </summary>
        /// <param name="textToErase">The text being erased</param>
        public void EraseText(string textToErase)
        {
            string resultOfPencilErase = Pencil.EraseText(textToErase);
            char[] paperAsCharArray = Paper.ToCharArray();

            int i = Paper.LastIndexOf(textToErase);
            int j = 0;
            while (j < resultOfPencilErase.Length)
            {
                paperAsCharArray[i] = resultOfPencilErase[j];
                i++;
                j++;
            }

            Paper = new string(paperAsCharArray);
        }

        /// <summary>
        ///     Find and Replace text on the paper with new text.  Finds the first occurance of the text beginning at the start of the paper.
        ///     While editing, any overwrites with result in writing an @ symbol
        /// </summary>
        /// <param name="oldText">The text that will be replaced</param>
        /// <param name="newText">The text that will replace the old text</param>
        public void EditText(string oldText, string newText)
        {
            string textPencilCanWrite = Pencil.WriteText(newText);
            char[] paperAsCharArray = Paper.ToCharArray();

            int i = Paper.IndexOf(oldText);
            int j = 0;
            while (j < textPencilCanWrite.Length)
            {
                if (paperAsCharArray[i] != ' ' && textPencilCanWrite[j] != ' ')
                {
                    paperAsCharArray[i] = '@';
                }
                else
                {
                    paperAsCharArray[i] = textPencilCanWrite[j];
                }
                
                i++;
                j++;
            }

            Paper = new string(paperAsCharArray);
        }
        #endregion
    }
}

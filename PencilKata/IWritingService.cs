﻿namespace PencilKata
{
    /// <summary>
    ///     The Writing Service Interface
    /// </summary>
    public interface IWritingService
    {
        #region Pencil
        /// <summary>
        ///     Assigns a pencil to the Writing Service
        /// </summary>
        /// <param name="pencil">The pencil that will be assigned</param>
        void AssignPencil(Pencil pencil);

        /// <summary>
        ///     Get a copy of the pencil assigned to the Writing Service
        /// </summary>
        Pencil GetPencil();
        #endregion

        #region Paper
        /// <summary>
        ///     Assigns paper to the Writing Service
        /// </summary>
        /// <param name="paper">The paper that will be assigned</param>
        void AssignPaper(string paper = "");

        /// <summary>
        ///     Get a copy of the paper assigned to the Writing Service
        /// </summary>
        string GetPaper();
        #endregion

        #region Editing
        /// <summary>
        ///     Write text on the Paper with the assigned Pencil
        /// </summary>
        /// <param name="textToWrite">The text that will be written to the paper with the pencil</param>
        void WriteTextOnPaper(string textToWrite);

        /// <summary>
        ///     Erase desired text from end of the paper
        /// </summary>
        /// <param name="textToRemove">The text being removed</param>
        void EraseText(string textToRemove);

        /// <summary>
        ///     Find and Replace text on the paper with new text.  Finds the first occurance of the text beginning at the start of the paper.
        /// </summary>
        /// <param name="oldText">The text that will be replaced</param>
        /// <param name="newText">The text that will replace the old text</param>
        void EditText(string oldText, string newText);
        #endregion
    }
}
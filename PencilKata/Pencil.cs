﻿using System;
using System.Text;

namespace PencilKata
{
    /// <summary>
    ///     The Pencil which is composed of a Point, Length, and Eraser each of which are DurableProperty types.
    /// </summary>
    public class Pencil
    {
        /// <summary>
        ///     The Point of the Pencil which is degraded by writing.  It may be restored.
        /// </summary>
        public DurableProperty Point { get; }

        /// <summary>
        ///     The Length of the Pencil which is degraded by sharpening.  It cannot be restored.
        /// </summary>
        public DurableProperty Length { get; }

        /// <summary>
        ///     The Eraser of the Pencil which is degraded by erasing.  It cannot be restored.
        /// </summary>
        public DurableProperty Eraser { get; }

        /// <summary>
        ///     The amount the eraser will degrade when erasing anything besides spaces
        /// </summary>
        private const int CostOfErasing = 1;

        /// <summary>
        ///     The amount the eraser will degrade when erasing spaces
        /// </summary>
        private const int CostOfErasingSpaces = 0;
        
        /// <summary>
        ///     The default amount the point will degrade when writing anything else
        /// </summary>
        private const int CostOfWriting = 1;

        /// <summary>
        ///     The default amount the point will degrade when writing uppercase letters
        /// </summary>
        private const int CostOfWritingUppercase = 2;

        /// <summary>
        ///     The amount the point will degrade when writing spaces
        /// </summary>
        private const int CostOfWritingSpaces = 0;
        
        /// <summary>
        ///     Constructor for the Pencil
        /// </summary>
        /// <param name="pointDurability">The initial durability value for the point</param>
        /// <param name="lengthDurability">The initial durability value for the length</param>
        /// <param name="eraserDurability">The initial durability value for the eraser</param>
        public Pencil(int pointDurability, int lengthDurability, int eraserDurability)
        {
            Point = new DurableProperty(pointDurability);
            Point.AllowRestore();
            Length = new DurableProperty(lengthDurability);
            Eraser = new DurableProperty(eraserDurability);
        }

        /// <summary>
        ///     Write text with the Pencil
        /// </summary>
        /// <param name="text">The requested text for the pencil to write</param>
        /// <returns>The actual text that will be written given the current pencil durability</returns>
        public string WriteText(string text)
        {
            char[] textToWrite = text.ToCharArray();
            StringBuilder resultText = new StringBuilder(string.Empty);

            int i = 0;
            while(i < textToWrite.Length)
            {
                if (CanWrite())
                {
                    Point.Degrade(GetCostOfWritingCharacter(textToWrite[i]));
                    resultText.Append(textToWrite[i]);
                }
                else
                {
                    resultText.Append(" ");
                }

                i++;
            }

            return resultText.ToString();
        }

        /// <summary>
        ///     Sharpen the Pencil and restore the point
        /// </summary>
        public void Sharpen()
        {
            if(CanBeSharpened())
            {
                Length.Degrade();
                Point.Restore();
            }            
        }

        /// <summary>
        ///     Erase text with the Pencil eraser, in the opposite order written, and return the result of erasing
        /// </summary>
        /// <param name="text">The text to be erased</param>
        public string EraseText(string text)
        {
            char[] textToErase = text.ToCharArray();
            StringBuilder resultText = new StringBuilder(string.Empty);

            int i = textToErase.Length - 1;
            while (i >= 0)
            {
                if (CanErase())
                {
                    Eraser.Degrade(GetCostOfErasingCharacter(textToErase[i]));
                    resultText.Insert(0, ' ');
                }
                else
                {
                    resultText.Insert(0, textToErase[i]);
                }

                i--;
            }

            return resultText.ToString();
        }

        /// <summary>
        ///     Evaluate the Point durability.  The Pencil can write if the Point is not degraded.
        /// </summary>
        public bool CanWrite()
        {
            return !Point.IsDegraded();
        }

        /// <summary>
        ///     Evaluate the Point and Length durability.  The Pencil can write if the Point is not degraded.
        /// </summary>
        public bool CanBeSharpened()
        {
            return Point.CanRestoreDurability && !Length.IsDegraded();
        }

        /// <summary>
        ///     Evaluate the Eraser durability.  The Pencil can erase if the Eraser is not degraded.
        /// </summary>
        /// <returns></returns>
        public bool CanErase()
        {
            return !Eraser.IsDegraded();
        }

        /// <summary>
        ///     Given a single character, determine if uppercase, lowercase, or space and return the cost to write that character.
        ///     Uppercase is 2, space is 0, everything else is 1
        /// </summary>
        /// <param name="character">The character being evaluated</param>
        private int GetCostOfWritingCharacter(char character)
        {
            if (char.IsUpper(character))
            {
                return CostOfWritingUppercase;
            }

            if (char.IsWhiteSpace(character))
            {
                return CostOfWritingSpaces;
            }

            return CostOfWriting;
        }
        
        /// <summary>
        ///     Given a single character, determine if it is a space or something else and return the cost to write that character.
        ///     A space is 0, everything else is 1
        /// </summary>
        /// <param name="character">The character being evaluated</param>
        private int GetCostOfErasingCharacter(char character)
        {
            if (char.IsWhiteSpace(character))
            {
                return CostOfErasingSpaces;
            }

            return CostOfErasing;
        }
    }
}

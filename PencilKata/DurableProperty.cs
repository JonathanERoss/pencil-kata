﻿using System;

namespace PencilKata
{
    /// <summary>
    ///     The DurableProperty represents a type which has a certain durability value that may degrade.
    ///     The ability to restore the durability is disabled by default but may be enabled to allow the 
    ///     CurrentDurability value to return to the initial value.
    /// </summary>
    public class DurableProperty
    {
        /// <summary>
        ///     The initial durability value for the property
        /// </summary>
        public int InitialDurability { get; }

        /// <summary>
        ///     The current durability value for the property
        /// </summary>
        public int CurrentDurability { get; private set; }

        /// <summary>
        ///     Indicates the ability of the property to restore it's durability 
        ///     rather than remaining degraded after the CurrentDurability becomes zero.
        /// </summary>
        public bool CanRestoreDurability { get; private set; }

        /// <summary>
        ///     Constructor for DurableProperty
        /// </summary>
        /// <param name="initialDurability">The initial durability value for the property</param>
        public DurableProperty(int initialDurability)
        {
            InitialDurability = initialDurability;
            CurrentDurability = initialDurability;
            DenyRestore();
        }

        /// <summary>
        ///     Degrade the Property by reducing the CurrentDurability by the supplied value.  
        ///     The CurrentDurability value will be prevented from becoming negative.
        /// </summary>
        /// <param name="value">How much the CurrentDurability should be degraded</param>
        public void Degrade(int value = 1)
        {
            int newDurability = CurrentDurability - value;
            CurrentDurability = newDurability > 0 ? newDurability : 0;
        }

        /// <summary>
        ///     Reset the CurrentDurability value back to the InitialDurability
        /// </summary>
        public void Restore()
        {
            if (CanRestoreDurability)
            {
                CurrentDurability = InitialDurability;
            }
            else
            {
                throw new InvalidOperationException("The Property is not enabled for restore");
            }
        }

        /// <summary>
        ///     Allow the Property to Restore it's Durability
        /// </summary>
        public void AllowRestore() => CanRestoreDurability = true;

        /// <summary>
        ///     Deny the Property from Restoring it's Durability
        /// </summary>
        public void DenyRestore() => CanRestoreDurability = false;

        /// <summary>
        ///     Evaluate the property and determine if it is degraded (current durability is zero)
        /// </summary>
        public bool IsDegraded() => CurrentDurability == 0;
    }
}

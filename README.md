# Pencil Kata
This repository contains my solution to the Code Kata programming exercise described [HERE](docs/KataSource.md)

## Overview
The solution was written as a .NET Core 2.0 class library using C#.  There are two projects. One for the class library and the other for unit tests.

- PencilKata
- PencilKata.Tests

The implementation contains a service called **WritingService** which provides an interface **IWritingService**.  Using this service, we can perform writing functions such as editing and erasing as well as writing text.  

To use the service, we simply assign a **Pencil** and paper.  The tests in the **WritingServiceTests** class provide examples of this.  One especially good example is the test named *AssignedPencilCanWriteThenWillDegradeAndCanBeSharpenedThenCanWriteAgain*.  This test shows how the consumer of the service can assign a pencil and do some writing.  When the pencil becomes dull it can be resharpened and the consumer can then begin editing where they left off and write more text.

My solution also provides a **Pencil** class which is composed of three **DurableProperty** classes for each of the pencil's main components: Point, Eraser, and Length.  The **DurableProperty** class allows for easily modeling the durability of each component, especially how they uniquely degrade or can be restored.  

The **Pencil** manages everything about itself, not just how the components behave, but more importantly how each of those affects the way the **Pencil** performs while assigned to the **WritingService**.

The class **PencilTests** contain tests to verify each of the characteristics of the **Pencil**.  The **DurableProperty** unit tests in the **DurablePropertyTests** class similarly verify the behavior of the components, but in a more generic way.

## Running The Tests

From the command line, run the following in the root of the repository:

    dotnet test .\PencilKata.Tests\PencilKata.Tests.csproj --verbosity normal

This will build the solution, then execute the tests.  You may also build using the following commands.

	dotnet clean
	dotnet build

The build step will fetch all nuget packages required of the projects.  It is not necessary, but to manually restore the packages simply execute the following command:

    dotnet restore

You may also open the solution file (PencilKata.sln) with Visual Studio and run tests.

## Test Results and Coverage

![Test Results](docs/TestResults.png)


## Author
- Jonathan E. Ross
